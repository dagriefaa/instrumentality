<div align="center">
<img src="header.png" alt="Instrumentality" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1509258433<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1509258433?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1509258433?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1509258433?color=%231b2838&logo=steam&style=for-the-badge)
</div>

Instrumentality is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds a *greatly* improved machine info display in build mode, and instrument readouts in simulation mode.

## Features
* The build info display improves on the vanilla display (and its predecessor, Mass Display) by:
    1. Expanding its size, allowing for a more sensible and readable layout instead of a tightly-wound and unsorted ball of numbers
    2. Adding more essential information (e.g. the machine position/rotation offset, which is not normally visible)
    3. Improving the accuracy of the Center of Mass display, and adding Center of Lift/Thrust displays
* The simulation instruments improve on contemporaries by:
    1. Updating at a consistent rate (10hz) independent of simulation speed
    2. Visible and expandable selection of display units (instead of random key + click combinations)
    3. Supporting clients in multiplayer!

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 12 (ish)/.NET Framework 3.5 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2019 (or later).
4. Press F6 to compile the mod.

## Usage
Usage instructions can also be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=1509258433).

<div align="center">![Build Readout](https://i.imgur.com/6R5avx7.png)</div>

* The block/mass displays show the block count/mass of the current selection, or the entire machine if nothing is selected.
* The Center of Lift (🟨 yellow) calculates the center of the forces exerted by Propellers, Small Propellers, Wing Blocks, and Wing Panels. Each axis is independent, and changes based on the direction of the Aerodynamic Drag display.
* The Center of Thrust (🟥 red) calculates the center of the forces exerted by Water Cannons, Flying Blocks, and Rockets. A sum vector is displayed showing the direction of thrust.
* The Aerodynamic Drag 💨 display (not shown) displays lift force for propellers and wing panels.

<div align="center">![Simulation Readout](https://i.imgur.com/sjiLVZm.png)</div>

Click on the units of the speedometer and altimeter to change them (e.g. kph, mph).

In order of priority, the displays show information for:
1. The current fixed camera block
2. The current free camera target
3. The first machine rigidbody
All numerical displays update 10 times per second, independent of time scale.

If you're using [Air Density Mod](https://gitlab.com/dagriefaa/airdensity), the air gauge will reflect the actual air density. Otherwise, it's a linear gauge between 0-2000m.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
