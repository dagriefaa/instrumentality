﻿using Besiege.UI.Extensions;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Instrumentality.BuildDisplay;
internal class MachineCenter : MachineCenterOfMass {

    static MachineCenterOfMass _original = null;
    protected static MachineCenterOfMass Original {
        get {
            if (!_original) {
                _original = Resources.FindObjectsOfTypeAll<MachineCenterOfMass>().Where(x => x is not MachineCenter).First();
            }
            return _original;
        }
    }

    new protected virtual void Awake() {
        base.Awake();
        BuildDisplayController.CenterModeChanged += ScheduleUpdate;
        BuildDisplayController.BlockSelectionChanged += ScheduleUpdate;

    }

    new protected virtual void OnDestroy() {
        base.OnDestroy();
        BuildDisplayController.CenterModeChanged -= ScheduleUpdate;
        BuildDisplayController.BlockSelectionChanged -= ScheduleUpdate;
    }

    // the actual schedule function is private...
    public void ScheduleUpdate() => OnParameterUndo(new UndoActionEdit(Machine.Active(), new BlockInfo(), new BlockInfo()));

    protected static bool UseSelection
        => BuildDisplayController.CurrentCenterMode is BuildDisplayController.CenterMode.SELECTION
            && AdvancedBlockEditor.Instance.CurrentState is not StatMaster.Tool.Modify
            && AdvancedBlockEditor.Instance.selectionController.MachineSelection.Count > 0;

    protected static List<BlockBehaviour> GetSelectedBlocksFor(Machine machine)
        => UseSelection
            ? AdvancedBlockEditor.Instance.selectionController.MachineSelection
            : machine.BuildingBlocks;

    internal static Transform MakeCenterVisual(string name, Color c, List<Quaternion> rotations = null) {

        Transform hudParent = HierarchyUtils.FindObject("HUD/3D");
        var linePrefab = HierarchyUtils.FindObject("HUD/3D/CenterOfMassVis/X/X").gameObject;

        Transform vis = HierarchyUtils.CreateObject(name, hudParent);

        // create lines
        rotations ??= new() { Quaternion.Euler(0, 0, 90), Quaternion.Euler(0, 0, 0), Quaternion.Euler(90, 0, 0) };
        foreach (Quaternion rot in rotations) {
            var line = new GameObject("Axis").transform;
            line.SetParent(vis, false);
            line.localRotation = rot;
            line.localScale = new Vector3(0.03f, 100, 0.03f);

            var line1 = GameObject.Instantiate(linePrefab, line, false) as GameObject;
            line1.transform.localPosition = new Vector3(0, -0.00093f, 0);
            line1.transform.localRotation = Quaternion.identity;

            var line2 = GameObject.Instantiate(linePrefab, line, false) as GameObject;
            line2.transform.localPosition = new Vector3(0, 0.00093f, 0);
            line2.transform.localRotation = Quaternion.Euler(0, 0, 180);
        }

        // color lines
        foreach (MeshRenderer lineRenderer in vis.GetComponentsInChildren<MeshRenderer>()) {
            lineRenderer.gameObject.layer = 0;
            MaterialPropertyBlock baseColorBlock = new();
            baseColorBlock.SetColor("_Color", new(0.25f, 0.25f, 0.25f, 0.5f));
            baseColorBlock.SetColor("_Emission", c);
            lineRenderer.SetPropertyBlock(baseColorBlock);

            // xray line
            GameObject xrayLine = GameObject.Instantiate(lineRenderer.gameObject, lineRenderer.transform.parent, false) as GameObject;
            xrayLine.layer = 23; // 3d hud
            MeshRenderer lineChildRenderer = xrayLine.GetComponent<MeshRenderer>();
            MaterialPropertyBlock xrayColorBlock = new();
            xrayColorBlock.SetColor("_Color", new(0.25f, 0.25f, 0.25f, 0.25f));
            xrayColorBlock.SetColor("_Emission", c);
            lineChildRenderer.SetPropertyBlock(xrayColorBlock);
        }

        vis.gameObject.AddComponent<ScaleRelativeToCamera>();
        vis.gameObject.SetActive(false);
        return vis.transform;
    }

    internal static Transform MakeArrowVisual(Transform parent, Color color) {
        // create force arrow
        var arrowPrefab = HierarchyUtils.FindObject("HUD/3D/CenterOfMassVis/DOWN/Vis").gameObject;
        var arrowParent = HierarchyUtils.CreateObject("Arrow", parent);
        arrowParent.transform.SetAsFirstSibling();
        var arrow = GameObject.Instantiate(arrowPrefab, arrowParent, false) as GameObject;
        arrow.transform.localRotation = Quaternion.Euler(90, 0, 0);
        arrow.transform.localScale = Vector3.one * 0.02f;
        var renderer = arrow.GetComponent<MeshRenderer>();
        renderer.ColorProperty("_Color", Color.gray);
        renderer.ColorProperty("_Emission", color);
        return arrowParent.transform;
    }
}
