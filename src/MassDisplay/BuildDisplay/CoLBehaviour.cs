﻿using Besiege.UI.Extensions;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Instrumentality.BuildDisplay;
internal class CoLBehaviour : MachineCenter {

    class Axis {
        public readonly Transform AxisVisual;
        public readonly Func<Vector3, float> PosAxis;

        public Vector3 TotalPosition;
        public float TotalPositionWeight;

        public Vector3 Position => TotalPositionWeight == 0 ? Vector3.zero : TotalPosition / TotalPositionWeight;


        public Axis(Transform axisVisual, Func<Vector3, float> posAxis) {
            AxisVisual = axisVisual;
            PosAxis = posAxis;
        }
        public void Reset() {
            TotalPosition = Vector3.zero;
            TotalPositionWeight = 0;
        }
    }

    List<Axis> axes;

    new void Awake() {
        base.Awake();
        StatMaster.Mode.AeroDisplayChanged += ScheduleUpdate;

        CenterOfMassVis = HierarchyUtils.CreateObject("CenterOfLift2", HierarchyUtils.FindObject("HUD/3D"));

        Color paleYellow = new(0.3f, 0.3f, 0.05f);
        axes = new() {
            new(MachineCenter.MakeCenterVisual("X", paleYellow, new(){Quaternion.Euler(0, 0, 90)}), v => v.y),
            new(MachineCenter.MakeCenterVisual("Y", paleYellow, new(){Quaternion.Euler(0, 0, 0)}), v => v.x),
            new(MachineCenter.MakeCenterVisual("Z", paleYellow, new(){Quaternion.Euler(90, 0, 0)}), v => v.z),
        };
        axes.Apply(x => {
            x.AxisVisual.SetParent(CenterOfMassVis, false);

            Action<MeshRenderer> SetupRenderers = x => {
                x.enabled = true;
                x.ColorProperty("_Color", new(0.5f, 0.2f, 0.3f));
                x.ColorProperty("_Emission", new(0.5f, 0.55f, 0.05f));
                x.ColorProperty("_RimColor", new(0.5f, 0.5f, 0.6f));
                x.FloatProperty("_RimPower", 0.85f);
            };

            var arrowParent = HierarchyUtils.CreateObject("Arrow", x.AxisVisual);
            arrowParent.localRotation = x.AxisVisual.GetChild(0).localRotation;

            var arrow1 = Instantiate(Original.BuoyancyUp.GetChild(0).gameObject, arrowParent, false) as GameObject;
            arrow1.name = "Up";
            arrow1.transform.localPosition = new Vector3(0, -0.04f, 0);
            arrow1.transform.localRotation = Quaternion.Euler(0, 0, 0);
            arrow1.transform.localScale = Vector3.one * 0.02f;
            arrow1.GetComponentsInChildren<MeshRenderer>().Apply(SetupRenderers);

            var arrow2 = Instantiate(arrow1, arrowParent, false) as GameObject;
            arrow2.name = "Down";
            arrow2.transform.localPosition = new Vector3(0, 0.04f, 0);
            arrow2.transform.localRotation = Quaternion.Euler(0, 0, 180);
            arrow2.GetComponentsInChildren<MeshRenderer>().Apply(SetupRenderers);
        });

        CenterOfBuoyancyVis = null;
        CenterOfMassVis.gameObject.SetActive(false);
    }

    new void OnDestroy() {
        base.OnDestroy();
        StatMaster.Mode.AeroDisplayChanged -= ScheduleUpdate;
    }

    protected override void CalculateCOM(Machine machine) {
        var blocks = GetSelectedBlocksFor(machine);

        if (blocks.Count == 0) {
            return;
        }

        axes.ForEach(a => a.Reset());

        blocks.ForEach(block => {
            if (block is not AxialDrag b) {
                return;
            }
            var xyz = Vector3.Scale(-b.upTransform.InverseTransformDirection(AeroDynamicDisplay.MovementDirection * AeroLine.DEFAULT_SPEED), b.AxisDrag);
            var liftDirection = b.transform.TransformDirection(xyz) * Mathf.Min(b.velocityCap * b.velocityCap, AeroLine.DEFAULT_SPEED * AeroLine.DEFAULT_SPEED);

            axes.ForEach(a => {
                float weight = Mathf.Abs(a.PosAxis(liftDirection));
                a.TotalPosition += b.GetCenter() * weight;
                a.TotalPositionWeight += weight;
            });
        });

        axes.ForEach(a => {
            a.AxisVisual.gameObject.SetActive(a.TotalPositionWeight != 0);
            if (a.TotalPositionWeight != 0) {
                a.AxisVisual.position = a.Position;
            }
        });
    }
}