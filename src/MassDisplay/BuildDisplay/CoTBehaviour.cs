﻿using Besiege.UI.Extensions;
using UnityEngine;

namespace Instrumentality.BuildDisplay;
internal class CoTBehaviour : MachineCenter {

    protected override void Awake() {
        base.Awake();
        Color paleRed = new(0.3f, 0.02f, 0.07f);
        CenterOfMassVis = MachineCenter.MakeCenterVisual("CenterOfThrust2", paleRed);

        arrowVisual = Instantiate(Original.BuoyancyUp.gameObject, CenterOfMassVis, false) as GameObject;
        arrowVisual.name = "Center";
        arrowVisual.transform.localPosition = Vector3.zero;
        arrowVisual.transform.localRotation = Quaternion.Euler(0, 0, 180);
        arrowVisual.transform.localScale = Vector3.one * 0.01f;
        arrowVisual.GetComponentsInChildren<MeshRenderer>(true).Apply(x => {
            x.enabled = true;
            x.ColorProperty("_Color", new(0.6f, 0, 0));
            x.ColorProperty("_Emission", new(0.7f, 0.1f, 0.25f));
            x.ColorProperty("_RimColor", new(0.15f, 0.65f, 0.65f));
            x.FloatProperty("_RimPower", 0.85f);
        });
        arrowVisual.transform.GetChild(0).localRotation = Quaternion.Euler(90, 0, 0);
    }

    GameObject arrowVisual;

    protected override void CalculateCOM(Machine machine) {
        var blocks = GetSelectedBlocksFor(machine);

        if (blocks.Count == 0) {
            return;
        }

        float totalWeight = 0;
        Vector3 center = Vector3.zero;
        Vector3 direction = Vector3.zero;

        void Add(BlockBehaviour block, float weight, Vector3 forceDir) {
            center += block.GetCenter() * weight;
            direction += forceDir * weight;
            totalWeight += weight;
        }
        foreach (var block in blocks) {
            if (block is WaterCannonController waterCannon) {
                var power = waterCannon.StrengthSlider.Value;
                Add(waterCannon, power <= 1 ? Mathf.Abs(power) : (1.4f * power - 0.4f) * 10f, block.transform.up * (power >= 0 ? 1 : -1));
            }
            else if (block is FlyingController flying) {
                var power = Mathf.Abs(flying.SpeedSlider.Value);
                Add(block, 100 * power - AeroLine.DEFAULT_SPEED * 1.5f, block.transform.forward * (power >= 0 ? 1 : -1));
            }
            else if (block is TimedRocket rocket) {
                var power = Mathf.Abs(rocket.PowerSlider.Value);
                Add(block, 500 * power * (1 - AeroLine.DEFAULT_SPEED / (52 * power)), block.transform.up * (power >= 0 ? 1 : -1));
            }
            else if (block is NauticalScrew screw && WaterController.Exist && screw.GetCenter().y < WaterController.waterTransformHeight + 2f) {
                var power = screw.speedSlider.Value;
                // weight = -(angular momentum).z * powerMultiplier * speedSlider
                Add(block, (-10.5f * 5 * power), block.transform.forward * (power >= 0 ? 1 : -1));
            }
        }

        CenterOfMassVis.position = center / totalWeight;
        arrowVisual.gameObject.SetActive(direction != Vector3.zero);
        if (direction != Vector3.zero) {
            arrowVisual.transform.rotation = Quaternion.LookRotation(direction);
            arrowVisual.transform.GetChild(0).localScale = Vector3.one * 3 * Mathf.Clamp01(direction.magnitude / 50f);
        }
    }
}