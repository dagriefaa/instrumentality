﻿using Besiege.UI;
using Besiege.UI.Bridge;
using Besiege.UI.Serialization;
using mattmc3.dotmore.Extensions;
using Modding;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Instrumentality.BuildDisplay;
public class BuildDisplayController : MonoBehaviour {

    readonly string[] massSliders = new[] { "mass", "buoyancy" };
    readonly string[] colliderToggles = new[] { "hascolliders", "opt-collider" };

    public enum CenterMode { SELECTION, ALL };
    public static CenterMode CurrentCenterMode = CenterMode.SELECTION;

    public static event Action BlockSelectionChanged;
    public static event Action CenterModeChanged;

    Text BlockCount;
    Text TotalBlockCount;
    Text MassCount;
    Text TotalMassCount;
    Text ClusterCount;

    Text MachinePosition;
    Text MachineRotation;
    Text MachineDimensions;

    Toggle KeymapOverview;
    public static Toggle ShowSimSignals { get; private set; } = null;
    public static Toggle ShowVariableOverview { get; private set; } = null;

    static Action<bool> SetSimSignal = null;
    static Action<bool> SetVariableOverview = null;

    CoMBehaviour COMCode;
    CoLBehaviour COLCode;
    CoTBehaviour COTCode;

    GameObject COBToggle;
    Text ServerBlockCount = null;
    Text LevelEntityCount = null;

    int lastBlockSelectionCount = -1;
    bool blockSelectionChanged = false;
    BlockBehaviour lastBlock = null;

    int lastEntitySelectionCount = -1;
    bool entitySelectionChanged = false;
    LevelEntity lastEntity = null;

    bool wasWater = false;
    bool closingKeymapper = false;

    bool isCampaignLevel = false;

    public static void EnableSimSignalButton(Action<bool> set) {
        SetSimSignal = set;
        if (ShowSimSignals != null) {
            ShowSimSignals.onValueChanged.AddListener(b => SetSimSignal(b));
            ShowSimSignals.gameObject.SetActive(true);
        }
    }

    public static void EnableVariableOverviewButton(Action<bool> set) {
        SetVariableOverview = set;
        if (ShowVariableOverview != null) {
            ShowVariableOverview.onValueChanged.AddListener(b => SetVariableOverview(b));
            ShowVariableOverview.gameObject.SetActive(true);
        }
    }

    static string FormatVector3(Vector3 v) {
        return $"{v.x:0.0}x, {v.y:0.0}y, {v.z:0.0}z";
    }

    void Start() {
        DontDestroyOnLoad(gameObject);

        ReferenceMaster.onMachineChanged += UpdateMachine;
        ReferenceMaster.onCalculateMiddle += UpdateMachine;
        ReferenceMaster.onMachineModified += UpdateMachine;
        BlockMapper.OnParameterChange += m => {
            if (Machine.Active() == null || Machine.Active().isSimulating) {
                return;
            }
            bool doUpdate = m switch {
                MSlider slider => slider.Key.Contains(massSliders, StringComparison.OrdinalIgnoreCase),
                MToggle toggle => toggle.Key.Contains(colliderToggles, StringComparison.OrdinalIgnoreCase),
                MMenu menu => menu.Key.Contains("surfmat", StringComparison.OrdinalIgnoreCase),
                _ => false
            };
            if (doUpdate) {
                UpdateMachine(Machine.Active());
            }
        };
        BlockMapper.OnParameterUndo += u => {
            if (Machine.Active() == null || Machine.Active().isSimulating) {
                return;
            }
            if (u is UndoActionField or UndoActionEdit) {
                UpdateMachine(Machine.Active());
            }
        };

        StatMaster.entityCountChanged += UpdateLevelEntities;
        StatMaster.totalBlocksChanged += UpdateTotalBlocks;

        Events.OnActiveSceneChanged += (x, y) => Create();
        Make.OnReady(Mod.MOD_NAME, Create);
    }

    void Create() {
        if (Mod.SceneNotPlayable())
            return;

        Transform original = GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Top Right Adjecent)/Buttons/INFO");
        original.gameObject.SetActive(false);

        Project project = Make.LoadProject(Mod.MOD_NAME, "BuildDisplay", original.parent);
        project.transform.localPosition = new Vector3(2.51f, -0.4f, 1.22f);

        COMCode = project.gameObject.AddComponent<CoMBehaviour>();
        COLCode = project.gameObject.AddComponent<CoLBehaviour>();
        COTCode = project.gameObject.AddComponent<CoTBehaviour>();

        BlockCount = project["BlockCount"].GetComponent<Text>();
        TotalBlockCount = project["TotalBlockCount"].GetComponent<Text>();
        MassCount = project["MassCount"].GetComponent<Text>();
        TotalMassCount = project["TotalMassCount"].GetComponent<Text>();
        ClusterCount = project["ClusterCount"].GetComponent<Text>();

        MachinePosition = project["MachinePosition"].GetComponent<Text>();
        MachineRotation = project["MachineRotation"].GetComponent<Text>();
        MachineDimensions = project["MachineDimensions"].GetComponent<Text>();

        Toggle aeroToggle = project["AeroDisplay"].GetComponent<Toggle>();

        project["CenterOfMass"].GetComponent<Toggle>().onValueChanged.AddListener(COMCode.ToggleCenterOfMass);
        project["CenterOfLift"].GetComponent<Toggle>().onValueChanged.AddListener(b => {
            COLCode.ToggleCOM(b);
            aeroToggle.isOn = b;
        });
        project["CenterOfThrust"].GetComponent<Toggle>().onValueChanged.AddListener(COTCode.ToggleCOM);

        COBToggle = project["CenterOfBuoyancy"]?.gameObject;
        COBToggle.GetComponent<Toggle>().onValueChanged.AddListener(COMCode.ToggleCentersOfBuoyancy);
        COBToggle.SetActive(WaterController.Exist);
        wasWater = WaterController.Exist;

        project["CenterMode"].GetComponent<Option>().options = Enum.GetNames(typeof(CenterMode)).ToList();
        project["CenterMode"].GetComponent<Option>().onValueChanged.AddListener(i => {
            CurrentCenterMode = (CenterMode)i;
            CenterModeChanged?.Invoke();
        });

        KeymapOverview = project["KeymapOverview"].GetComponent<Toggle>();
        KeymapOverview.onValueChanged.AddListener(b => {
            if (Machine.Active() != null && !closingKeymapper) {
                if (OverviewBlockMapper.CurrentInstance == null) { OverviewBlockMapper.Open(Machine.Active()); }
                else { OverviewBlockMapper.Close(); }
            }
            closingKeymapper = false; // prevent keymapper reopening when closed via window
        });
        ShowSimSignals = project["KeysInSimulation"].GetComponent<Toggle>();
        if (SetSimSignal != null) {
            ShowSimSignals.onValueChanged.AddListener(b => SetSimSignal(b));
            ShowSimSignals.gameObject.SetActive(true);
        }
        else {
            ShowSimSignals.gameObject.SetActive(false);
        }
        ShowVariableOverview = project["VariableOverview"].GetComponent<Toggle>();
        if (SetVariableOverview != null) {
            ShowVariableOverview.onValueChanged.AddListener(b => SetVariableOverview(b));
            ShowVariableOverview.gameObject.SetActive(true);
        }
        else {
            ShowVariableOverview.gameObject.SetActive(false);
        }

        aeroToggle.onValueChanged.AddListener(b => StatMaster.Mode.displayDrag = b);
        project["ResetAero"].GetComponent<Button>().onClick.AddListener(AeroDynamicDisplay.Reset);

        var clusterIcon = project["ClusterIcon"].GetComponent<Image>();
        var clusterTooltip = project["ClusterTooltip"].GetComponent<Text>();
        var costIcon = project["CostIcon"].GetComponent<Image>();
        var costTooltip = project["CostTooltip"].GetComponent<Text>();
        isCampaignLevel = StatMaster.GetCurrentIsland() is not Island.None or Island.WaterSandbox;
        if (isCampaignLevel) {
            clusterIcon.enabled = false;
            clusterTooltip.enabled = false;
            costIcon.enabled = true;
            costTooltip.enabled = true;
        }
        else {
            clusterIcon.enabled = true;
            clusterTooltip.enabled = true;
            costIcon.enabled = false;
            costTooltip.enabled = false;
        }

        // multiverse displays
        project["Multiverse"].gameObject.SetActive(StatMaster.isMP);
        if (StatMaster.isMP) {
            LevelEntityCount = project["LevelEntityCount"].GetComponent<Text>();
            ServerBlockCount = project["ServerBlockCount"].GetComponent<Text>();
            UpdateTotalBlocks();
        }
    }

    void UpdateMachine(Machine m) {
        if (!m || m != Machine.Active() || !TotalBlockCount || !TotalMassCount)
            return;

        StatMaster.BlockCount = m.DisplayBlockCount;
        int blockSelectionCount = AdvancedBlockEditor.Instance?.selectionController.MachineSelection
            .Where(x => x.Prefab.Type != BlockType.BuildNode && x.Prefab.Type != BlockType.BuildEdge) // no surface nodes thanks
            .Count() ?? 0;

        // block counters
        TotalBlockCount.gameObject.SetActive(blockSelectionCount > 0);
        if (blockSelectionCount > 0) {
            BlockCount.text = $"{blockSelectionCount}";
            TotalBlockCount.text = $"{StatMaster.BlockCount}";
        }
        else {
            BlockCount.text = $"{StatMaster.BlockCount}";
        }

        // mass counters
        TotalMassCount.gameObject.SetActive(blockSelectionCount > 0);
        if (blockSelectionCount > 0) {
            try {
                float selectionMass = AdvancedBlockEditor.Instance.selectionController.MachineSelection
                    .Where(b => b.Prefab.Type is not (BlockType.Pin or BlockType.CameraBlock or BlockType.BuildNode or BlockType.BuildEdge))
                    .Sum(b => b.Rigidbody.mass);
                MassCount.text = $"{selectionMass:0.000}";
                TotalMassCount.text = $"{m.Mass:0.00}";
            }
            catch (Exception e) {
                Debug.LogWarning("Exception in mass calculation section: \n " +
                    $"States: {m} {MassCount} {TotalMassCount} {AdvancedBlockEditor.Instance} {AdvancedBlockEditor.Instance.selectionController} "
                    + e.Message, this);
            }
        }
        else {
            MassCount.text = $"{m.Mass:0.00}";
        }

        // cluster count
        ClusterCount.text = $"{(isCampaignLevel ? m.BlocksCost : m.ClusterCount)}";

        // dimensions
        Bounds b;
        try {
            b = m.GetBounds();
        }
        catch (NullReferenceException e) {
            Debug.LogWarning($"Exception in getting machine bounds: " + e.Message, this);
            b = new Bounds();
        }
        MachineDimensions.text = FormatVector3(b.size);

        // buoyancy
        if (WaterController.Exist != wasWater) {
            COBToggle.SetActive(WaterController.Exist);
            wasWater = WaterController.Exist;
        }
    }

    void UpdateTotalBlocks() {
        if (!StatMaster.isMP) {
            return;
        }
        ServerBlockCount.text = $"{Playerlist.Players.Where(x => x.machine).Sum(x => x.machine.DisplayBlockCount)}";
    }

    void UpdateLevelEntities(int count) {
        if (!StatMaster.isMP) {
            return;
        }
        int selectionCount = LevelEditor.Instance?.selectionController.Selection.Count ?? 0;
        if (selectionCount > 0) {
            LevelEntityCount.text = $"{selectionCount}/{count}";
        }
        else {
            LevelEntityCount.text = $"{count}";
        }
    }

    void Update() {
        if (Mod.SceneNotPlayable())
            return;
        if (!Machine.Active() || !Machine.Active().CanModify || ReferenceMaster.activeMachineSimulating)
            return;

        // update machine offsets
        if (MachinePosition && MachinePosition.isActiveAndEnabled) {
            MachinePosition.text = FormatVector3(Machine.Active().BuildingMachine.localPosition);
            MachineRotation.text = FormatVector3(Machine.Active().BuildingMachine.localEulerAngles);
        }

        // update keymap button
        if (!OverviewBlockMapper.IsOpen && KeymapOverview.isOn) {
            closingKeymapper = true;
            KeymapOverview.isOn = false;
        }

        // block selection count
        if (lastBlockSelectionCount != AdvancedBlockEditor.Instance.selectionController.Selection.Count
            || AdvancedBlockEditor.Instance.selectionController.MachineSelection.FirstOrDefault() != lastBlock
            || blockSelectionChanged) {
            if (InputManager.LeftMouseButtonHeld()) {
                blockSelectionChanged = true;
            }
            else {
                lastBlockSelectionCount = AdvancedBlockEditor.Instance.selectionController.Selection.Count;
                UpdateMachine(Machine.Active());
                BlockSelectionChanged?.Invoke();
                blockSelectionChanged = false;
            }
        }
        lastBlock = AdvancedBlockEditor.Instance.selectionController.MachineSelection.FirstOrDefault();

        if (LevelEditor.Instance) {
            // entity selection count
            if (lastEntitySelectionCount != LevelEditor.Instance.selectionController.Selection.Count
                || LevelEditor.Instance.selectionController.LevelSelection.FirstOrDefault() != lastEntity
                || entitySelectionChanged) {
                if (InputManager.LeftMouseButtonHeld()) {
                    entitySelectionChanged = true;
                }
                else {
                    lastEntitySelectionCount = LevelEditor.Instance.selectionController.Selection.Count;
                    UpdateLevelEntities(LevelEditor.Instance.Entities.Count);
                    entitySelectionChanged = false;
                }
            }
            lastEntity = LevelEditor.Instance.selectionController.LevelSelection.FirstOrDefault();
        }
    }
}