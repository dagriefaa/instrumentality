﻿using Besiege.UI.Extensions;
using SRF;
using System.Collections.Generic;
using UnityEngine;

namespace Instrumentality.BuildDisplay;
internal class CoMBehaviour : MachineCenter {

    bool comEnabled = false;
    bool buoyancyEnabled = false;

    protected override void Awake() {
        base.Awake();

        CenterOfMassVis = MachineCenter.MakeCenterVisual("CenterOfMass2", new(0.02f, 0.07f, 0.3f));
        var arrow = Instantiate(Original.BuoyancyUp.gameObject, CenterOfMassVis, false) as GameObject;
        arrow.name = "Center";
        arrow.transform.localPosition = Vector3.zero;
        arrow.transform.localRotation = Quaternion.Euler(0, 0, 180);
        arrow.transform.localScale = Vector3.one * 0.01f;
        arrow.GetComponentsInChildren<MeshRenderer>().Apply(x => {
            x.enabled = true;
            x.ColorProperty("_Color", new(0, 0.3f, 0.85f));
            x.ColorProperty("_Emission", new(0.2f, 0, 0.5f));
            x.ColorProperty("_RimColor", new(0.5f, 0.8f, 1));
            x.FloatProperty("_RimPower", 0.85f);
        });
        CenterOfMassVis.GetChildren().Apply(x => x.gameObject.SetActive(false));

        CenterOfBuoyancyVis = Original.CenterOfBuoyancyVis;
        CenterOfBuoyancyVis.GetComponentsInChildren<MeshRenderer>().Apply(x => {
            x.enabled = false;
            x.ColorProperty("_Color", new(0.1f, 0.5f, 0.3f));
            x.ColorProperty("_Emission", new(0, 0.3f, 0.6f));
            x.ColorProperty("_RimColor", new(0.6f, 0.2f, 0.8f));
        });

        BuoyancyDown = Original.BuoyancyDown;
        BuoyancyDown.GetComponentsInChildren<MeshRenderer>().Apply(x => x.enabled = false);

        BuoyancyUp = Original.BuoyancyUp;
        BuoyancyUp.GetComponentsInChildren<MeshRenderer>().Apply(x => x.enabled = false);

        Original.enabled = false;
    }

    void Start() {
        CenterOfBuoyancyVis.localScale = Vector3.one * 0.01f;
        CenterOfBuoyancyVis.GetChild(0).localScale = Vector3.one * 3f;
        CenterOfBuoyancyVis.gameObject.AddComponent<ScaleRelativeToCamera>();
        BuoyancyDown.localScale = Vector3.one * 0.01f;
        BuoyancyDown.gameObject.AddComponent<ScaleRelativeToCamera>();
    }

    public void ToggleCenterOfMass(bool enabled) {
        comEnabled = enabled;
        CenterOfMassVis.transform.GetChildren().Apply(x => x.gameObject.SetActive(enabled));
        ToggleCOM(comEnabled || buoyancyEnabled);
    }

    public void ToggleCentersOfBuoyancy(bool enabled) {
        buoyancyEnabled = enabled;
        CenterOfBuoyancyVis.GetComponentsInChildren<MeshRenderer>().Apply(x => x.enabled = enabled);
        BuoyancyDown.GetComponentsInChildren<MeshRenderer>().Apply(x => x.enabled = enabled);
        BuoyancyUp.GetComponentsInChildren<MeshRenderer>().Apply(x => x.enabled = enabled);
        ToggleCOM(comEnabled || buoyancyEnabled);
    }

    protected override void CalculateCOM(Machine machine) {
        if (!UseSelection) {
            base.CalculateCOM(machine);
            return;
        }

        // MachineCenterOfMass uses Machine.buildingBlocks
        // we want to be able to limit it to currently selected blocks
        // Machine.buildingBlocks is copy-only, but is directly exposed through ReferenceMaster for some reason
        // so jank workaround is to empty it, copy in the selection, do the calculation, and then fill it back up

        var machineBlocks = ReferenceMaster.BuildingBlocks[machine.PlayerID];
        var machineBlocksCopy = new List<BlockBehaviour>(machineBlocks);
        machineBlocks.Clear();
        machineBlocks.AddRange(AdvancedBlockEditor.Instance.selectionController.MachineSelection);
        base.CalculateCOM(machine);
        machineBlocks.Clear();
        machineBlocks.AddRange(machineBlocksCopy);
    }
}