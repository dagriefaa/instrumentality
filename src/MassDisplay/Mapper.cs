﻿using Instrumentality.SimDisplay;
using ObjectExplorer.Mappings;
using UnityEngine;

namespace Instrumentality; 

public class Mapper : MonoBehaviour {

    void Awake() {

        ObjectExplorer.ObjectExplorer.AddMappings("Instrumentality",
            new MIterable<SimDisplayController, ushort>("MachineData", c => SimDisplayController.machines.Keys),

            new MFloat<RadialGauge>("Value", c => c.Value, (c, v) => c.SetValue(v)),
            new MBool<RadialGauge>("ShowNegative", c => c.ShowNegative, (c, v) => c.ShowNegative = v),

            new MFloat<MonospaceNumber>("Value", c => c.Value, (c, v) => c.SetValue(v)),
            new MInt<MonospaceNumber>("DecimalPlaces", c => c.DecimalPlaces, (c, v) => c.SetDecimalPlaces(v)),

            new MBool<SlideAnimation>("Extended", c => c.Extended, (c, v) => c.SetExtended(v)),
            new MVector3<SlideAnimation>("Delta", c => c.delta, (c, v) => c.delta = v),
            new MFloat<SlideAnimation>("AnimationTime", c => c.animationTime, (c, v) => c.animationTime = v)
        );

        //ObjectExplorer.ObjectExplorer.AddShortcut(() => GameObject.Find("HUD").transform.FindChild("TopBar/Mover/Align (Top Right Adjecent)/Buttons/INFO/fold out").gameObject);

        Destroy(this);
    }
}
