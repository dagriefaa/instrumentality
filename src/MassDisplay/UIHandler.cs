﻿using Besiege.UI;
using Besiege.UI.Serialization;
using Instrumentality.BuildDisplay;
using Instrumentality.SimDisplay;
using Modding;
using UnityEngine;

namespace Instrumentality;
internal class UIHandler : MonoBehaviour {
    void Start() {
        Make.RegisterSerialisationProvider(Mod.MOD_NAME, new SerializationProvider {
            CreateText = c => Modding.ModIO.CreateText(c),
            GetFiles = c => Modding.ModIO.GetFiles(c),
            ReadAllText = c => Modding.ModIO.ReadAllText(c),
            AllResourcesLoaded = () => ModResource.AllResourcesLoaded,
            OnAllResourcesLoaded = e => ModResource.OnAllResourcesLoaded += e
        });

        Make.OnReady(Mod.MOD_NAME, () => {
            Make.RegisterSprite(Mod.MOD_NAME, "ClusterIcon", ModResource.GetTexture("ClusterIcon"));
            Make.RegisterSprite(Mod.MOD_NAME, "DimensionsIcon", ModResource.GetTexture("DimensionsIcon"));
            Make.RegisterSprite(Mod.MOD_NAME, "LevelObjectsIcon", ModResource.GetTexture("LevelObjectsIcon"));
            Make.RegisterSprite(Mod.MOD_NAME, "TotalBlocksIcon", ModResource.GetTexture("TotalBlocksIcon"));
            Make.RegisterSprite(Mod.MOD_NAME, "Gauge", ModResource.GetTexture("Gauge"));
            Make.RegisterSprite(Mod.MOD_NAME, "GaugeMask", ModResource.GetTexture("GaugeMask"));
            Make.RegisterSprite(Mod.MOD_NAME, "SpeedGauge", ModResource.GetTexture("SpeedGauge"));
            Make.RegisterSprite(Mod.MOD_NAME, "AccelGauge", ModResource.GetTexture("AccelGauge"));
            Make.RegisterSprite(Mod.MOD_NAME, "AltitudeGauge", ModResource.GetTexture("AltitudeGauge"));
            Make.RegisterSprite(Mod.MOD_NAME, "AirGauge", ModResource.GetTexture("AirGauge"));
            Make.RegisterSprite(Mod.MOD_NAME, "Knob", ModResource.GetTexture("Knob"));
            Make.RegisterSprite(Mod.MOD_NAME, "IceMarker", ModResource.GetTexture("IceMarker"));
            Make.RegisterSprite(Mod.MOD_NAME, "LogicIcon", ModResource.GetTexture("LogicIcon"));
            Make.RegisterSprite(Mod.MOD_NAME, "CenterIconHalf", ModResource.GetTexture("CenterIconHalf"));
            Make.RegisterSprite(Mod.MOD_NAME, "CostIcon", ModResource.GetTexture("CostIcon"));

            Mod.ModControllerObject.AddComponent<BuildDisplayController>();
            Mod.ModControllerObject.AddComponent<SimDisplayController>();
        });
        Destroy(this);
    }
}
