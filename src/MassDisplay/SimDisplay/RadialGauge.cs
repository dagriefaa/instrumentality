﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Instrumentality.SimDisplay; 
internal class RadialGauge : MonoBehaviour {

    const int SWEEP = 15; // angle per step
    const int INITIAL_OFFSET = -2;

    class GaugeNumber {
        public Transform pivot;
        public Text numberText;
        public int offset;

        public GaugeNumber(Transform pivot, Text numberText, int offset) {
            this.pivot = pivot;
            this.numberText = numberText;
            this.offset = offset + INITIAL_OFFSET;
        }
    }

    public float Value { get; private set; } = 0;

    public float Increment = 25; // value step size
    public int DecimalPlaces = 0;
    public bool ShowNegative = true;

    Transform rotor;
    List<GaugeNumber> numbers = new List<GaugeNumber>();

    void Start() {
        rotor = transform.Find("GaugeRotor");
        foreach (Text t in GetComponentsInChildren<Text>(true)) {
            numbers.Add(new GaugeNumber(t.transform.parent, t, numbers.Count));
        }
    }

    public void SetValue(float value) {
        Value = value;
        rotor.localEulerAngles = new Vector3(0, 0, value / Increment * SWEEP);
        foreach (var num in numbers) {
            UpdateNumber(value, num);
        }
    }

    void UpdateNumber(float value, GaugeNumber number) {
        value += Increment * 0.5f;
        float angle = value % Increment / Increment * SWEEP;
        if (angle < 0) { angle += SWEEP; } // account for negative angles
        angle += number.offset * -SWEEP - SWEEP * 0.5f;
        number.pivot.localEulerAngles = new Vector3(0, 0, angle);

        float displayValue = number.offset * Increment + Mathf.Floor(value / Increment) * Increment;
        number.numberText.enabled = ShowNegative || displayValue >= 0;
        number.numberText.text = displayValue.ToString("F" + DecimalPlaces);
    }
}
