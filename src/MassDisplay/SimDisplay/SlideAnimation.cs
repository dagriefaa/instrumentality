﻿using System.Collections;
using UnityEngine;

namespace Instrumentality.SimDisplay; 
public class SlideAnimation : MonoBehaviour {

    public Vector3 delta;
    public float animationTime = 0.5f;
    public float waitTime = 0;

    public bool Extended { get; private set; } = false;

    bool isExtended;
    bool animating;
    Vector3 initialPosition;

    private void Start() {
        initialPosition = this.transform.localPosition;
    }

    public void SetExtended(bool extended) {
        Extended = extended;
        if (Extended == isExtended) {
            return;
        }
        StopAllCoroutines();
        StartCoroutine(IEAnimate(Extended, waitTime, animationTime));
    }

    private IEnumerator IEAnimate(bool extend, float wait, float duration) {
        isExtended = extend;
        if (!animating) {
            yield return new WaitForSecondsRealtime(wait);
        }
        animating = true;
        Vector3 target = !extend ? initialPosition : (initialPosition + delta);
        for (float t = 0f; t < duration; t += Time.unscaledDeltaTime) {
            this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, target, t / duration);
            yield return null;
        }
        this.transform.localPosition = target;
        animating = false;
        yield break;
    }
}
