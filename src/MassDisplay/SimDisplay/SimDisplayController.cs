﻿using Besiege.UI;
using Modding;
using Modding.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Instrumentality.SimDisplay; 
public class SimDisplayController : MonoBehaviour {

    GameObject lastTargetObject = null;
    public static GameObject TargetObject { get; private set; } = null;
    public static Rigidbody TargetRigidbody { get; private set; } = null;

    public class VelocityData {
        public float acceleration = 0.01f;
        public float speed = 0f;
        public Vector3 velocity = Vector3.zero;

        public override string ToString() {
            return $"{speed}, {acceleration}, {velocity}";
        }
    }

    public static Dictionary<ushort, VelocityData> machines = new Dictionary<ushort, VelocityData>();

    Transform root;
    SlideAnimation slideAnimation;

    RadialGauge speedRuler;
    MonospaceNumber speedValue;
    Dropdown speedUnit;
    public enum SpeedUnit { Metres, Kilometres, Miles, Mach, Knots }
    public static readonly string[] SpeedUnitTextStrings
        = new string[] { "MPS", "KPH", "MPH", "MACH", "KTS" };
    public static readonly float[] SpeedMultipliers = new float[] { 1f, 3.6f, 2.2369f, 0.00292f, 1.944f };
    public static readonly float[] SpeedIncrements = new float[] { 25, 100, 50, 0.25f, 50 };

    UnityEngine.UI.Slider accelGauge;
    MonospaceNumber accelValue;
    public enum AccelUnit { Metres, Gs }
    public static readonly string[] AccelUnitTextStrings = new string[] { "MPS²", "G" };
    public static readonly float[] AccelMultipliers = new float[] { 1f, 0.1019f };

    RadialGauge altitudeRuler;
    MonospaceNumber altitudeValue;
    Dropdown altitudeUnit;
    public enum AltUnit { Metres, Feet }
    public static readonly string[] AltUnitTextStrings = new string[] { "M", "FT" };
    public static readonly float[] AltMultipliers = new float[] { 1f, 3.2808f };
    public static readonly float[] AltIncrements = new float[] { 25, 100 };


    UnityEngine.UI.Slider airGauge;
    UnityEngine.UI.Slider iceGauge;
    MonospaceNumber airValue;
    IceController ice;

    SpeedUnit _speed = SpeedUnit.Metres;
    public SpeedUnit CurrentSpeedUnit {
        get {
            return _speed;
        }
        set {
            _speed = value;
            Configuration.GetData().Write("CurrentSpeedUnit", (int)value);
        }
    }

    AccelUnit _accel = AccelUnit.Metres;
    public AccelUnit CurrentAccelUnit {
        get {
            return _accel;
        }
        set {
            _accel = value;
            Configuration.GetData().Write("CurrentAccelUnit", (int)value);
        }
    }

    AltUnit _alt = AltUnit.Metres;
    public AltUnit CurrentAltUnit {
        get {
            return _alt;
        }
        set {
            _alt = value;
            Configuration.GetData().Write("CurrentAltUnit", (int)value);
        }
    }

    public static Func<Transform, float> AirDensityFunction = t => 1f - AltitudeFunction(t) / 2000f;
    public static Func<Transform, float> AltitudeFunction = x => x.position.y;

    void Start() {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(RunRepeating(RunDisplays, 0.1f));

        CurrentSpeedUnit = (SpeedUnit)(!Configuration.GetData().HasKey("CurrentSpeedUnit") ? 0 : Configuration.GetData().ReadInt("CurrentSpeedUnit"));
        CurrentAccelUnit = (AccelUnit)(!Configuration.GetData().HasKey("CurrentAccelUnit") ? 0 : Configuration.GetData().ReadInt("CurrentAccelUnit"));
        CurrentAltUnit = (AltUnit)(!Configuration.GetData().HasKey("CurrentAltUnit") ? 0 : Configuration.GetData().ReadInt("CurrentAltUnit"));

        machines[0] = new VelocityData();

        Events.OnActiveSceneChanged += (x, y) => Create();
        Create();

    }

    void Create() {
        if (Mod.SceneNotPlayable())
            return;

        // find ice layer
        IceController[] iceLayers = Resources.FindObjectsOfTypeAll<IceController>();
        if (iceLayers.Length > 0) {
            float min = iceLayers.Min(x => x.transform.position.y);
            ice = iceLayers.Where(x => x.transform.position.y == min).First();
        }

        root = new GameObject("BottomLeft").transform;
        root.SetParent(GameObject.Find("HUD").transform, false);
        var alignUI = root.gameObject.AddComponent<AlignToScreenPoint>();
        alignUI.alignLeft = true;
        alignUI.alignOnStart = true;

        var project = Make.LoadProject(Mod.MOD_NAME, "SimDisplay", root.transform);
        project.transform.localPosition = new Vector3(0, 0, 18);

        slideAnimation = project["SlideAnimator"].GetComponent<SlideAnimation>();

        // speed
        speedRuler = project["SpeedRuler"].GetComponent<RadialGauge>();
        speedValue = project["SpeedValue"].GetComponent<MonospaceNumber>();
        speedUnit = project["SpeedUnit"].GetComponent<Dropdown>();
        speedUnit.ClearOptions();
        speedUnit.AddOptions(SpeedUnitTextStrings.ToList());
        speedUnit.value = (int)CurrentSpeedUnit;
        speedUnit.onValueChanged.AddListener(x => {
            CurrentSpeedUnit = (SpeedUnit)x;
            ConfigureSpeedGauges();
        });
        ConfigureSpeedGauges();

        // acceleration
        accelValue = project["AccelValue"].GetComponent<MonospaceNumber>();
        accelGauge = project["AccelGauge"].GetComponent<UnityEngine.UI.Slider>();

        // altitude
        altitudeRuler = project["AltitudeRuler"].GetComponent<RadialGauge>();
        altitudeValue = project["AltitudeValue"].GetComponent<MonospaceNumber>();
        altitudeUnit = project["AltitudeUnit"].GetComponent<Dropdown>();
        altitudeUnit.ClearOptions();
        altitudeUnit.AddOptions(AltUnitTextStrings.ToList());
        altitudeUnit.value = (int)CurrentAltUnit;
        altitudeUnit.onValueChanged.AddListener(x => {
            CurrentAltUnit = (AltUnit)x;
            ConfigureAltitudeGauges();
        });
        ConfigureAltitudeGauges();

        // air density
        airGauge = project["AirGauge"].GetComponent<UnityEngine.UI.Slider>();
        airValue = project["AirValue"].GetComponent<MonospaceNumber>();

        // ice
        iceGauge = project["IceGauge"].GetComponent<UnityEngine.UI.Slider>();
    }

    void ConfigureSpeedGauges() {
        if (speedValue && speedRuler) {
            if (CurrentSpeedUnit == SpeedUnit.Mach) {
                speedValue.SetDecimalPlaces(3);
                speedRuler.DecimalPlaces = 2;
            }
            else {
                speedValue.SetDecimalPlaces(1);
                speedRuler.DecimalPlaces = 0;
            }
            speedRuler.Increment = SpeedIncrements[(int)CurrentSpeedUnit];
        }
    }

    void ConfigureAltitudeGauges() {
        if (altitudeRuler) {
            altitudeRuler.Increment = AltIncrements[(int)CurrentAltUnit];
        }
    }

    void Update() {
        if (Mod.SceneNotPlayable()) {
            return;
        }
        slideAnimation.SetExtended(!(Machine.Active() && Machine.Active().isSimulating));
        if (!root.gameObject.activeSelf) {
            return;
        }

        if (!StatMaster.isClient && !TargetObject) {
            return;
        }

        Transform target = StatMaster.isClient
            ? Player.GetLocalPlayer()?.Machine?.SimulationMachine?.GetChild(0)
            : TargetObject.transform;

        if (!target) {
            return;
        }

        // update gauges
        if (speedRuler.isActiveAndEnabled) {
            float multipliedSpeed = machines[0].speed * SpeedMultipliers[(int)CurrentSpeedUnit];
            speedRuler.SetValue(multipliedSpeed);
        }
        if (accelGauge.isActiveAndEnabled) {
            accelGauge.value = Mathf.Lerp(accelGauge.value, machines[0].acceleration, 0.5f);
        }
        if (altitudeRuler.isActiveAndEnabled) {
            altitudeRuler.SetValue(AltitudeFunction(target) * AltMultipliers[(int)CurrentAltUnit]);
        }
        if (airGauge.isActiveAndEnabled) {
            airGauge.value = AirDensityFunction(target);
            if (ice && ice.isActiveAndEnabled) {
                iceGauge.value = AirDensityFunction(ice.transform);
            }
            else {
                iceGauge.handleRect.GetComponent<Image>().enabled = false;
            }
        }
    }

    void FixedUpdate() {
        if (Mod.SceneNotPlayable()) {
            return;
        }
        if (StatMaster.isClient) {
            return;
        }

        if (Machine.Active() && Machine.Active().isSimulating) {
            // get target
            // camera block
            if (FixedCameraController.Instance.activeCamera) {
                TargetObject = FixedCameraController.Instance.activeCamera.gameObject;
                if (TargetObject != lastTargetObject) {
                    TargetRigidbody = TargetObject.GetComponentInParent<Rigidbody>();
                }
            }
            // camera target - block, entity, or others with rigidbody
            else if (MouseOrbit.Instance.targetType != MouseOrbit.TargetType.Machine) {
                TargetObject = MouseOrbit.Instance.target.gameObject;
                if (TargetObject != lastTargetObject) {
                    TargetRigidbody = TargetObject.GetComponentInParent<Rigidbody>();
                }
            }
            // default to first rigidbody if there is no target
            if (!TargetRigidbody) {
                TargetRigidbody = Machine.Active().SimulationMachine?.GetComponentInChildren<Rigidbody>();
                TargetObject = TargetRigidbody.gameObject;
            }
            lastTargetObject = TargetObject;

            if (!TargetRigidbody) {
                return;
            }
        }

        // get info
        if (StatMaster.isMP) { // multiplayer
            Player.GetAllPlayers().ConvertAll(x => x.Machine?.InternalObjectServer).ForEach(x => {
                if (!x) { return; }
                UpdateMachineVelocity(x, x.PlayerID);
                if (machines.ContainsKey(x.PlayerID)) {
                    ModNetworking.SendTo(Player.From(x.player), Messages.Velocity.CreateMessage(new object[] {
                        machines[x.PlayerID].speed,
                        machines[x.PlayerID].acceleration,
                        machines[x.PlayerID].velocity
                    }));
                }
            });
        }
        else if (Machine.Active()) {
            UpdateMachineVelocity(Machine.Active(), 0);
        }

    }

    void UpdateMachineVelocity(Machine m, ushort id) {
        if (!m || !m.isSimulating) {
            return;
        }

        Rigidbody target = id == 0 ? TargetRigidbody : m.SimulationMachine.GetComponentInChildren<Rigidbody>();
        if (!target) {
            return;
        }

        if (!machines.ContainsKey(id)) {
            machines.Add(id, new VelocityData());
        }
        machines[id].velocity = target.velocity;
        float currentSpeed = machines[id].velocity.magnitude;
        float actualAcceleration = (currentSpeed - machines[id].speed) / Time.fixedDeltaTime;
        if (Time.timeScale > Mathf.Epsilon) {
            machines[id].speed = currentSpeed;
            machines[id].acceleration = Mathf.Abs(actualAcceleration);
        }
    }

    void RunDisplays() {
        if (Mod.SceneNotPlayable()) {
            return;
        }
        if (!ReferenceMaster.activeMachineSimulating) {
            return;
        }

        float multipliedAcceleration = machines[0].acceleration * AccelMultipliers[(int)CurrentAccelUnit];
        float multipliedSpeed = machines[0].speed * SpeedMultipliers[(int)CurrentSpeedUnit];
        if (accelValue.isActiveAndEnabled) {
            accelValue.SetValue(multipliedAcceleration);
        }
        speedValue.SetValue(multipliedSpeed);

        if (!StatMaster.isClient && !TargetObject) {
            return;
        }

        Transform target = StatMaster.isClient
            ? Player.GetLocalPlayer()?.Machine.SimulationMachine.GetChild(0)
            : TargetObject.transform;

        if (!target) {
            return;
        }

        Vector3 position = target.position;
        if (airValue.isActiveAndEnabled) {
            airValue.SetValue(Mathf.Clamp(AirDensityFunction(target) * 100, 0, 100));
        }
        altitudeValue.SetValue(AltitudeFunction(target) * AltMultipliers[(int)CurrentAltUnit]);
    }

    public static IEnumerator RunRepeating(Action action, float seconds) {
        for (; ; ) {
            action?.Invoke();
            yield return new WaitForSecondsRealtime(seconds);
        }
    }
}