﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Instrumentality.SimDisplay; 
internal class MonospaceNumber : MonoBehaviour {

    public float Value { get; private set; } = 0;
    public int DecimalPlaces = 1;

    List<Text> digits = new List<Text>();
    Text point;

    void Start() {
        foreach (Text t in GetComponentsInChildren<Text>()) {
            if (t.name == "Point") {
                point = t;
            }
            else if (t.name == "Digit") {
                digits.Add(t);
            }
        }
        digits.Reverse();
        SetDecimalPlaces(DecimalPlaces);
    }

    public void SetValue(float value) {
        Value = value;
        int displayValue = Mathf.FloorToInt(value * Mathf.Pow(10, DecimalPlaces));
        for (int i = 0; i < digits.Count; i++) {
            digits[i].text = Mathf.Abs(i < digits.Count - 1 ? (displayValue % 10) : displayValue).ToString();
            digits[i].color = displayValue == 0 && i > DecimalPlaces ? new Color(1, 1, 1, 0.5f) : Color.white;
            if (value < 0) {
                digits[i].color *= new Color(1, 0.4f, 0.4f, 1);
            }
            displayValue /= 10;
        }
        point.color = digits[0].color;
    }

    public void SetDecimalPlaces(int places) {
        if (!point) {
            DecimalPlaces = places;
            return;
        }
        DecimalPlaces = Mathf.Clamp(places, 0, digits.Count - 1);
        point.transform.SetSiblingIndex(digits.Count - DecimalPlaces);
        point.enabled = DecimalPlaces > 0;
        SetValue(Value);
    }
}
