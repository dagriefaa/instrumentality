using Instrumentality.SimDisplay;
using Modding;
using Modding.Blocks;
using System;
using UnityEngine;

namespace Instrumentality;
public class Mod : ModEntryPoint {

    public const string MOD_NAME = "Instrumentality";


    public static GameObject ModControllerObject;

    public override void OnLoad() {
        ModControllerObject = GameObject.Find("ModControllerObject");
        if (!ModControllerObject) UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject"));

        if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
            ModControllerObject.AddComponent<Mapper>();
        }

        // networking
        Messages.Velocity = ModNetworking.CreateMessageType(new DataType[] {
            DataType.Single, // speed
            DataType.Single, // acceleration
            DataType.Vector3 // velocity
        });
        ModNetworking.Callbacks[Messages.Velocity] += msg => {
            SimDisplayController.machines[0].speed = (float)msg.GetData(0);
            SimDisplayController.machines[0].acceleration = (float)msg.GetData(1);
            SimDisplayController.machines[0].velocity = (Vector3)msg.GetData(2);
        };

        Events.OnBlockInit += AddAeroDisplay;

        // fix for spring/winch mass being incorrect
        Events.OnBlockInit += b => {
            if (b.InternalObject is SpringCode spring && b.Machine == PlayerMachine.GetLocal() && !ReferenceMaster.activeMachineSimulating) {
                if (spring.Prefab.Type == BlockType.RopeWinch) {
                    spring.MassSlider.ValueChanged += m => {
                        if (!ReferenceMaster.activeMachineSimulating) {
                            spring.Rigidbody.mass = m * 2;
                            spring.startBody.mass = m;
                            spring.endBody.mass = m;
                        }
                    };
                }
                else {
                    spring.Rigidbody.mass = spring.startBody.mass + spring.endBody.mass;
                }
            }
        };

        if (Mods.IsModLoaded(new Guid("61d89dcf-88a2-4a16-8eb2-08aeed441f1d"))) {
            ModControllerObject.AddComponent<UIHandler>();
        }
    }
    public static bool SceneNotPlayable() {
        return !AdvancedBlockEditor.Instance;
    }

    private void AddAeroDisplay(Modding.Blocks.Block b) {
        if (b.Machine.InternalObject != Machine.Active() || b.Machine.InternalObject.isSimulating) {
            return;
        }
        if (b.InternalObject is AxialDrag) {
            GameObject lineParent = new GameObject("AeroLine");
            lineParent.transform.SetParent(b.GameObject.transform, false);
            lineParent.AddComponent<AeroLine>();
        }
    }
}
