﻿using UnityEngine;

namespace Instrumentality;
class AeroLine : MonoBehaviour {

    // todo - use surface aero lines

    public const float DEFAULT_SPEED = 30f;

    LineRenderer line;

    static Material foregroundMaterial;

    AxialDrag self;

    void Awake() {

        self = GetComponentInParent<AxialDrag>();

        if (!foregroundMaterial) {
            foregroundMaterial = new Material(Shader.Find("Particles/Alpha Blended Ztest"));
            foregroundMaterial.SetFloat("_ZGreaterAlpha", 0.3f);
        }

        if (ReferenceMaster.activeMachineSimulating) {
            if (StatMaster.isClient) {
                DestroyImmediate(this.gameObject);
                return;
            }
            line = GetComponent<LineRenderer>();
        }
        else {
            line = this.gameObject.AddComponent<LineRenderer>();
            line.material = foregroundMaterial;
            line.SetWidth(0.15f, 0);
            var lineColor = new Color(1, 1, 1, 0.25f);
            line.SetColors(lineColor, lineColor);
        }
    }

    void Update() {
        if (!StatMaster.Mode.displayDrag || StatMaster.hudHidden) {
            line.enabled = false;
            return;
        }
        line.enabled = true;

        Vector3 xyz;
        if (ReferenceMaster.activeMachineSimulating) {
            Vector3 a = self.upTransform.InverseTransformDirection(self.Rigidbody.velocity);
            xyz = Vector3.Scale(-a, self.AxisDrag) * Mathf.Min(self.Rigidbody.velocity.sqrMagnitude, self.velocityCap * self.velocityCap) / 100f;


            float distToCap = Mathf.Clamp(1 - (self.Rigidbody.velocity.sqrMagnitude / self.velocityCap * self.velocityCap), 0, 1);
        }
        else {
            Vector3 a = self.upTransform.InverseTransformDirection(AeroDynamicDisplay.MovementDirection * DEFAULT_SPEED);
            xyz = Vector3.Scale(-a, self.AxisDrag) * self.velocityCap / 3f;
        }
        Vector3 aeroCenter = self.GetCenter();
        line.SetPositions(new[] { aeroCenter, aeroCenter + this.transform.TransformDirection(xyz) });
    }
}
