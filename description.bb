[img=https://i.imgur.com/KW1oFiN.png][/img]
                             [b][url=https://besiege.fandom.com]Besiege Wiki[/url][/b]  |  [b][url=https://gitlab.com/dagriefaa/instrumentality]GitLab Repository[/url][/b]

[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2913469777][img]https://i.imgur.com/XGCiJRy.png[/img][/url]

[img=https://i.imgur.com/aIRQLEn.png][/img]

[img]https://i.imgur.com/sjiLVZm.png[/img]

[h1]Build Mode[/h1]
- Adds mass counts, machine position, machine rotation, and machine dimensions, and reorganises everything sensibly.
- If blocks or entities are selected, the relevant counters will show how many are selected as well as the overall totals.
- The position/rotation offset is how much the whole machine was moved without selecting anything.
- This mod adds more physics centers (and separates the water centers from the CoM).
--- The Center of Lift has independent axes and changes with airflow direction. The further behind the CoM it is, the more stable your plane will be.
--- The Center of Thrust calculates for thruster blocks (i.e. flying spirals, water cannons, and rockets). The thrust vector should point through the center of mass.
- The aero display shows lift force for propellers and wing panels. Rotate the display in build mode to change the angle of simulated movement.

[h1]Simulation Mode[/h1]
- Adds a speedometer, altimeter, accelerometer, and barometer.
- In order of priority, the displays show information for:
[olist]
    [*] The current fixed camera block
    [*] The current free camera target
    [*] The first placed machine block
[/olist]
- All numerical displays update 10 times per second, [b]independent of time scale[/b].
- Click the units on the speedometer and altimeter to change them.
- If you're using [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1747633774&comment=airDensity]Air Density Mod[/url], the barometer will display the current air pressure.
--- If you don't, it will display the current altitude (capped to 2000 metres).